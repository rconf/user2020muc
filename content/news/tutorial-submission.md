+++
title = "Call for Tutorials"
date = "2020-01-13T08:00:00+02:00"
tags = ["submission", "tutorials", "programme"]
categories = ["submission"]
banner = "img/banners/tutorial-submission-preview.jpg"
author = "Michel Lang"
description = "Submission for a 3 hour tutorial at UseR! 2020 European Hub"
summary = "We welcome proposals for tutorials for the UseR! 2020 satellite event in Munich on July 7-10. We will consider any topic as long as the tutorial targets a broad audience from the R community. We will host three tutorials running in parallel for approximately 3 hours."
+++


We welcome proposals for tutorials for the UseR! 2020 satellite event in Munich on July 7-10. We will consider any topic as long as the tutorial targets a broad audience from the R community. We will host three tutorials running in parallel for approximately 3 hours.<!--more-->

Submissions should be sent as PDF (no more than 1 page please) to <a href="mailto:programme-useR2020muc@stat.uni-muenchen.de">programme-useR2020muc@stat.uni-muenchen.de</a> and contain the following information:

- Title
- Name of the presenter(s)
- Abstract (~ 300 words)
- Target audience
- Prerequisities
- Link to the project homepage (if applicable)

 Deadline for tutorial submission is **February 16th, 2020**.
