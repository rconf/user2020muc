+++
title = "Be a Sponsor!"
date = "2020-01-16T16:00:00+02:00"
tags = ["sponsoring"]
categories = ["finance"]
banner = "img/banners/sponsoring.jpeg"
author = "Daniel Schalk"
description = "Information about sponsoring the event"
summary = "Be a sponsor of the first ever UseR! 2020 European Hub and connect with a community of one of the leading data science tools"
+++


We want to offer the participants of the useR!2020 European Hub -- graduate students, academic staff, and professors as well as private- and public-sector employees -- the best possible conference experience. In order to achieve this goal we need you as a sponsor. This role will make you and your company or organization highly visible in a community of one of the industry-leading data science tools. Further, it will allow you to be in contact with interested job seekers, and academic or industry partners.

All sponsors will be mentioned in the welcome talk and will be promoted on

- the website,
- the staff t-shirts,
- the speaker desk, and
- the default presentation slides shown between sessions.

Additionally, it is possible to get a table in the lobby where you are free to promote yourself and get in touch with the participants.

<center>
    <div style="width:60%;margin:80px;padding:10px;border:5px solid #467fbf;font-size:18px;background:rgb(70, 127, 205, 0.05);">
        <b>
            Get in touch with us and be part of the first ever UseR! 2020 European Hub!<br>Contact us via <a href="mailto:finance-useR2020muc@stat.uni-muenchen.de">finance-useR2020muc@stat.uni-muenchen.de</a>.
        </b>
    </div>
</center>

