+++
title = "Call for Abstracts"
date = "2020-01-27T09:00:00"
tags = ["Abstracts", "Submission"]
categories = ["Abstracts", "Submission"]
banner = "img/banners/abstract-submission-preview.jpg"
author = "Andreas Bender"
+++

The submission portal is now open! (https://www.conftool.org/user2020muc)

useR!2020 invites R enthusiasts from around the world to submit abstracts for the 2020 conference in Munich, Germany. The meeting will feature different formats and a diverse field of speakers and participants. There will be plenty of
opportunities for discussions, networking and spending some quality time in Bavaria's capital.<!--more--> Abstracts can be submitted in one of the following categories

- Oral presentations
- Lightning Talks
- Posters

Due to limitations on the number of talks available at the conference, submissions for Oral presentations will also be considered for a Lightning talk or a poster presentation. Lightning talk submissions will also be considered for a poster presentation.

Additionally, we offer the
opportunity to submit abstracts for a "breakout" session. Details on the individual formats are given below.

## Submission Process

All submissions (except for [tutorials](../../../../2020/01/13/tutorial-submission/)) are processed through the conference
registration and submission portal:

1. Create a new user account at https://www.conftool.org/user2020muc
2. Log in using your credentials
3. Go to "Your Submissions"
4. Select one of the four formats
5. Fill out the submission form



## Oral Presentations

Oral presentations will be 20 minutes. Please consider the following guidelines when submitting an abstract for an oral presentation.

1. Talks should be new to useR!2020. If a talk was presented at a previous useR! conference or another major conference in Europe, then we will judge the abstract on the added contribution since the previous talk.
2. Talks should be originating from the presenter. Talks discussing other people's packages may be considered for lightning talks or posters, but not generally for oral presentations.
3. Most, if not all, successful submissions for oral presentations will have a working and active repository (if a package), a technical report that is available at the time of review, or an available preprint or accepted paper at the time of review. If this does not apply to your work, please consider submitting a lightning talk or poster.
4. Novel case studies are allowed, but typically will be funneled toward lightning talks or posters.
5. Work should be directly related to R. General data science talks are not typically appropriate for oral presentations, though some exceptions may be made.

## Lightning Talks and Posters

Lightning talks are 5 minutes, and are a great way to advertise your work! Or, meet and greet people interested in your work at a poster session. This is where anything related to R that may not quite fit in the oral presentation category is fair game. We would love to see creative and interesting ideas for these.

## Breakout sessions

These sessions offer an alternative way to contribute to the conference that
potentially go beyond the technical level or don't quite fit in one of the other
formats. For example, breakout sessions could be used for an invited session or workshop on a particular topic or to provide information
to the broader R community about topics like diversity and inclusion.


## List of topics from which presenters can choose

1. algorithms
2. applications
3. big/high dimensional data
4. bioinformatics
5. biostatistics/epidemiology
6. community/education
7. data mining
8. databases/data management
9. economics/finance/insurance
10. models
11. multivariate analysis
12. other
13. performance
14. programming
15. R in production
16. reproducibility
17. spatial
18. statistics in social sciences
19. time series
20. visualisation
21. web app


