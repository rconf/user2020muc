+++
title = "Message to the community: Our considerations regarding COVID-19"
date = "2020-03-20T13:00:00"
tags = ["Abstracts", "Submission", "Programme", "Tutorials", "Important"]
categories = ["General"]
banner = "img/banners/covid.jpg"
author = "Heidi Seibold"
+++

We are all affected by the corona crisis and there is no exception for the useR! 2020 European Hub conference and the conference organizing team. In this message to the community we would like to make the current state of our considerations transparent.<!--more -->

None of us know what the upcoming months will be like. We are all hoping that in July everything will have gone back to normal, but it is simply not possible to know. Other conferences already decided to go virtual, postpone, or cancel.

**useR! 2020 European Hub is a satellite conference of useR! 2020 in St. Louis, USA. As such we will heavily depend on the decisions made by the main hub.** We are in close contact with the US conference organizers and the R Foundation Conference Committee. We are trying to decide as soon as possible but at the same time with as much information as possible. We expect to be able to give you a definitive answer mid May.

We already have received many great submissions with which we will be able to create an amazing conference programme (plus of course the great [keynotes and tutorials](https://user2020muc.r-project.org/programme/)!).
If you would like to contribute, you can still submit abstracts (see our [call for abstracts](https://user2020muc.r-project.org/news/2020/01/27/abstract-submission/) and the [extended deadline](https://user2020muc.r-project.org/news/2020/03/16/abstract-extention/)).

Until we have made a final decision we ask all potential attendees and speakers to not make any travel plans.

For questions on tickets you have bought already, we refer to our terms of registration and payment (see below).

In times when the best you can do for the people you care about is to stay at a distance, we hope to care for the R community by being as transparent as possible. Please contact us on [Twitter](https://twitter.com/useR2020muc) or via [email](https://user2020muc.r-project.org/contact/) if you have questions.

Stay safe!

Heidi and the useR! 2020 European Hub team

---

Terms of Registration and Payment

- The registration of participation is binding.
- If you have to cancel the registration, you get 80% back until 30 days before the start of the event. In all other cases, the financial responsibilities of the participants remain fully effective.
- The participation fees are owed upon registration and are payable within 7 days following submission of the registration (but not later than 7 days before the starting day of the event).
- Participation is not guaranteed until full payment of the registration fee is received.
- The conference program may be subject to changes.
- Payments will be refunded if the conference will be canceled by the organizer. In that case, the organizer will have no further liability to the client. Registrations remain valid if the conference has to be postponed.
