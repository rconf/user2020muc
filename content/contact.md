+++
title = "Contact"
description = "Contact information"
keywords = ["contact", "get in touch", "email"]
+++

<b>We'd <i class="far fa-heart"></i> to help!</b> Please contact us if you have any further questions:

<p style="margin-bottom: 40px;"></p>

<center>

|                                                       |                                                   |
|:----------------------------------------------------- |:------------------------------------------------- |
| **Finance and Sponsoring** <i style="margin-left: 50px;"></i>                                          | <a href="mailto:finances-useR2020muc@stat.uni-muenchen.de">finances-useR2020muc@stat.uni-muenchen.de</a>       |
| **Social**                                            | <a href="mailto:social-useR2020muc@stat.uni-muenchen.de">social-useR2020muc@stat.uni-muenchen.de</a>          |
| **Publicity**                                         | <a href="mailto:publicity-useR2020muc@stat.uni-muenchen.de">publicity-useR2020muc@stat.uni-muenchen.de</a>       |
| **Registration**    | <a href="mailto:registration-useR2020muc@stat.uni-muenchen.de">registration-useR2020muc@stat.uni-muenchen.de</a>    |
| **Logistics**                                         | <a href="mailto:logistics-useR2020muc@stat.uni-muenchen.de">logistics-useR2020muc@stat.uni-muenchen.de</a>       |
| **Volunteers**                                        | <a href="mailto:volunteers-useR2020muc@stat.uni-muenchen.de">volunteers-useR2020muc@stat.uni-muenchen.de</a>      |
| **Technical**                                         | <a href="mailto:it-useR2020muc@stat.uni-muenchen.de">it-useR2020muc@stat.uni-muenchen.de</a>              |
| **Programme**                                         | <a href="mailto:programme-useR2020muc@stat.uni-muenchen.de">programme-useR2020muc@stat.uni-muenchen.de</a>       |
| **Other**                                         | <a href="mailto:user2020muc@r-project.org">user2020muc@r-project.org</a>       |

</center>

<p style="margin-bottom: 40px;"></p>

Emails are sent to the responsible team ([see here](../#organising)).

<p style="margin-bottom: 40px;"></p>

