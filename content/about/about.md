+++
title = "About"
description = "Information about the organisers"
keywords = ["about","organisers","staff","R consortium"]
+++

### <i class="fas fa-globe-europe"></i> What is the useR! European hub?

This conference is a satellite event of the useR! 2020. The [main event](https://user2020.r-project.org/) is hosted in St. Louis, USA. The scientific programme will include tutorials, talks, breakout sessions, lightning talks, and a poster session. We will have some shared sessions with the conference in St. Louis using video streaming and online tools for communication.

This satellite event is a pilot project for the upcoming hub and spokes model that useR! conferences are planning to implement starting in 2021. With this we hope to contribute to a reduction of flights.

### <i class="fas fa-users"></i> What is the useR! conference?

Actively supported by the [R Foundation](https://www.r-project.org/conferences/), the useR! is an annual, international conference of leading statisticians and data scientists from
around the world. In recent years, it has been held in Toulouse, France; Brisbane, Australia;
Brussels, Belgium; and Stanford, California. The conference is the main annual meeting of
the R community, and attendance in 2020 is anticipated to be over 1,000 people. Attendees
will include R developers and users who are data scientists, business intelligence specialists,
analysts, statisticians from academia and industry, and students.

<br>

### <i class="fab fa-r-project"></i> What is R?

R is a programming language that offers a free, open source platform for data analytics,
statistics, visualization, geospatial analyses, mapping, scientific communication, and
dashboard creation. It is one of the most popular tools in data science, with over 50% of
data analysis professionals using R in their workflow.

<br>

<!--
### <i class="fas fa-map-marker-alt"></i> When and where will the conference be held?

The conference will be held in Downtown St. Louis at the [Marriott St. Louis Grand Hotel](/venue/) and
Conference Center from July 7th to July 10th, 2020.

<br>
-->

###  <i class="fas fa-envelope-open-text"></i> How can I stay up to date?

Please check back often, as our website will be updated regularly. You can also [follow us on Twitter](https://twitter.com/useR2020muc).

<br>

### <i class="fas fa-hand-holding-usd"></i> Are there sponsorship opportunities?

Sponsorship of useR! 2020 European hub is an opportunity to support your data science team
development, underwrite the continued development of a tool in your analytics pipeline,
and continue to connect your company to the R user community. It is also an opportunity to
connect with future data science hires who may be interested in joining your team. If you are
interested in details about sponsorship, please [contact](/contact_new/) the organizing team.


<br>

### <i class="fas fa-baby-carriage"></i> How to get support when participating with children?

We aim to make this event attractive for parents with kids. If you intend to attend with a child, please contact <a href="mailto:social-useR2020muc@stat.uni-muenchen.de">social-useR2020muc@stat.uni-muenchen.de</a>.

<br>

### <i class="fab fa-accessible-icon"></i> <i class="fas fa-blind"></i> <i class="fas fa-deaf"></i> How to get support when participating with disabilities?

We aim to make this event as inclusive as we can. The venue is wheelchair-accessible, as is public transport to the venue from the main station. We will waive registration fees for personal service assistants. If you have any questions please check our [FAQ on accessibility](/accessibility/). Please notify the organizers of your requirements so that we can plan ahead. To do so please contact <a href="mailto:social-useR2020muc@stat.uni-muenchen.de">social-useR2020muc@stat.uni-muenchen.de</a>.
