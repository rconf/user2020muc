+++
title = "mlr3"
description = "Tutorial mlr3 Bernd Bischl & Michel Lang"
keywords = ["programme", "tutorial"]
+++

__Bernd Bischl__ & __Michel Lang__

Our tutorial introduces the `mlr3` package (https://mlr3.mlr-org.com) for modern,
state-of-the-art machine learning in R.
The mlr3 ecosystem provides a one-stop solution for all machine learning (ML) needs,
spanning preprocessing, model learning and evaluation, ensembles, visualization, and
hyperparameter tuning. Its pipeline system `mlr3pipelines` allows to easily express
complex workflows, and to quickly prototype new ideas and applications. Whether you
are applying ML to solve a practical prediction problem, implementing learning
algorithms as a research software engineer or researching ML by empirical means,
`mlr3` can help make your workflow more readable and efficient.

### Learning Objectives

The main objective of the tutorial is to introduce and familiarize users with `mlr3` and its ecosystem. This will allow participants to take advantage of its functionality for their own projects, in particular:

- how to benchmark and compare different machine learning approaches in a statistically sound manner,
- how to build complex machine learning workflows, including preprocessing and stacked ensembles,
- automatic hyperparameter tuning and pipeline optimization (AutoML),
- how to get the technical "nitty-gritties" for ML experiments right, e.g., speed up by parallelization, encapsulation of experiments in external processes or working on databases.

After the tutorial, participants will be able to implement complex solutions to real-world machine learning problems, and to evaluate the different design decisions necessary for such implementations, in particular the choice between different modelling and preprocessing techniques.


### Target Audience

We target to users with at least basic knowledge of machine learning concepts who are not yet familiar with `mlr3`. We require a working knowledge of R at medium level. Being able to work with tabular data is essential (subsetting, summarizing, transforming) and one should know R's general type system and basic operations.

