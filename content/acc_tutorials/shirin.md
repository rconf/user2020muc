+++
title = "Deep Learning with Keras and TensorFlow"
description = "Tutorial Deep Learning with Keras and TensorFlow Dr. Shirin Elsinghorst"
keywords = ["programme", "tutorial"]
+++

__Dr. Shirin Elsinghorst__

This workshop begins with a short theoretical part to make everyone familiar with the basics of
deep learning, including cross-entropy, loss, activation functions and how weights and biases
are optimized with backpropagation and gradient descent. Then we will delve into how to build
(deep) neural networks with Keras and TensorFlow in R. We will prepare data, train sequential
models, make predictions on test data and evaluate model performance. In order to get a broad
overview, we will look into simple MLPs trained on a tabular dataset, an image classification
task using CNNs and, if time allows, discuss a text classification example using LSTMs.
During the entire workshop, we will work with open-source datasets.

The tutorial will be a short version of the following workshop: https://www.codecentric.de/wissen/schulung/deep-learning-mit-keras-und-tensorflow/

### Target Audience

R users with a solid understanding of the basics of machine learning, looking to get started with the
TensorFlow package in R.

### Prerequisities

- Latest R version installed
- Latest RStudio version installed
- Latest versions of packages `keras` and `tidyverse` installed

