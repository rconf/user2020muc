+++
title = "Analyzing and visualising spatial and spatiotemporal data cubes"
description = "Tutorial Spatial and Spatiotemporal Data Cubes Edzer Pebesma & Martijn Tennekes"
keywords = ["programme", "tutorial"]
+++

__Edzer Pebesma__ & __Martijn Tennekes__

Data cubes are a modern way to denote array data, and we focus on array data
where some of the dimensions refer to space and time. Examples are spatial raster data,
multivariate time series for multiple locations, and time series of raster images such as
satellite data or weather predictions. In this workshop we will show a variety of cases where
such data arise, and demonstrate how they can be analysed and visualised with R. While
doing so, we mainly focus on using the R packages sf, stars, tmap and mapview, but also
address a number of other useful packages for this context.

Link to the project homepage: http://github.com/r-spatial/useR2020muc

### Target Audience

R users, who are somewhat familiar with (or have a demand for learning
about) spatial data handling in R, and who are interested in analysing and dealing with
raster data and spatiotemporal data cubes.

### Prerequisities

Some familiarity with R and R-spatial, as well as some prior knowledge about
time series and spatial data analysis is recommended, but is not a hard requirement.

