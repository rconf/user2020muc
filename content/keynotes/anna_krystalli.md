+++
title = "Computational Reproducibility, from theory to practice"
description = "Keynote Computational Reproducibility Anna Krystalli"
keywords = ["programme", "keynote"]
+++

The Reproducibility Crisis has received much attention in the past decade and has even been proposed as the biggest challenge of our academic generation. While debate about the extent and implications of the crisis continues, few would argue that more transparent research, where the raw materials underlying findings are openly shared, would not improve reliability and is, therefore, a reasonable expectation of modern research.

While strides have been made in promoting such expectations and the long journey of building the infrastructure, culture and capacity to materialise it have begun in earnest, we still have some way to go to move from reproducibility the concept to reproducibility the practice.

Drawing on experiences as a Research Software Engineer, an editor and reviewer of R packages for rOpenSci and a founding member of the ReproHack reproducibility hackathon team, I'll take a look at the progress we've made so far, try to identify what we're still missing and offer some thoughts on how to move forward.

### About the Author

<p>
<b>Anna Krystalli</b><br>
Research Software Engineer<br>
University of Sheffield, UK
</p>
<p style="margin-bottom:30px;">
Anna is a Research Software Engineer (RSE) at the University of Sheffield with a background in Marine Macroecology and research software development in R. She is part of a team of RSEs working to help researchers build more robust analysis pipelines and software, promote best practice in research programming and digital resource management and facilitate the shift to more open, transparent and collaborative research culture.  She is also an editor for rOpenSci, a 2019 Software Sustainability Institute Fellow and a co-organiser of the Sheffield R Users group. Overall, her passion lies in helping researchers and the research community as a whole make better use of the real workhorses of research, code and data, and in spreading the word about the joys of R.
</p>
