+++
title = "CRAN 2030"
description = "Keynote CRAN 2030 Uwe Ligges and Kurt Hornik"
keywords = ["programme", "keynote"]
+++

We will look at how some of CRAN's principles and mechanisms
have evolved over the past, and what they might turn into during the
next decade.  We provide both our personal views on challenges and
opportunities, and try to answer some key questions, including:

* What and who is CRAN?   How does CRAN exist physically?
* What is the point in having CRAN maintainers keep package maintainers so busy fixing new problems?
* What are the technical solutions to keep a repository of more than 15,000 packages up and running?  How does CRAN incoming and regular checking work?
* What are the human resources that were and are needed for maintaining CRAN?  How can package maintainers help keeping the workload feasible?
* What are the necessary changes for the future in order to keep the CRAN package repository as successful as it is today?
* What will happen during the next 10 years of CRAN?

### About the Authors

<p>
<b>Dr. Uwe Ligges</b><br>
Department of Statistics <br>
TU Dortmund University
</p>

- 1998 first contact with R
- 1999 first mailing list activities
- 2002 first binary built for CRAN
- 2005 first book about R
- 2011 R Core member
- 2020 important CRAN 2030 talk

<p style="margin-top:30px;">
<b>Univ.Prof. Dipl.-Ing. Dr. Kurt Hornik</b><br>
Institute for Statistics and Mathematics,<br>
Department of Finance, Accounting and Statistics,<br>
Wirtschaftsuniversität Wien,<br>
Welthandelsplatz 1, 1020 Wien, Austria
</p>

- 1987 PhD in Applied Mathematics
- 1990 Habilitation in "Statistics in its mathematical foundations"
- 1993 gained write access to the R sources and subsequently became R Core team
- 1997 founded CRAN together with Fritz Leisch
- 2003 Full professor of Statistics at WU Wirtschaftsuniversität Wien

<p style="margin-bottom:30px;">
</p>
