+++
title = "Talk with your model! Towards the language for exploration and explanation of machine learning models."
description = "Keynote Talk with your model! Przemysław Biecek"
keywords = ["programme", "keynote"]
+++

It is getting easier to build complex predictive models. However, without proper verification, these models carry a hidden risk that they will eventually stop working and we won't even notice. To prevent this from happening we need good diagnostic tools to explore, maintain and debug models. During the talk I will present set of methods used to examine the behaviour of predictive models. I will also show how to use these methods to build more robust and reliable models. Presented use-cases come from the book ,,Explanatory Model Analysis. Explore, Explain and Examine Predictive Models' https://github.com/pbiecek/ema.'

### About the Author

<p>
<b>Przemysław Biecek</b><br>
Associate Professor at Warsaw University of Technology,<br>
Principal Data Scientist at Samsung R&D Institute Poland<br>
</p>
<p style="margin-bottom:30px;">
Przemek graduated in mathematical statistics and software engineering. Then he fell in love with R. Since then he has been working on predictive models mostly with applications in oncology.  In such applications we need to understand models that are in use and that is why his current obsession is to visualize, explore and debug models. In his free time Przemek is a big believer of data-literacy. He has written four books about R, data visualisation and statistical modeling. Recently he started a Beta-Bit project with data-oriented games, which takes place in R console. Look for the BetaBit package on CRAN.
</p>
