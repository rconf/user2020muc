+++
title = "Programme"
description = "What's happening at the event"
keywords = ["programme","schedule","events","topics"]
+++

<style>
.row::after {
    content: "";
    clear: both;
    display: table;
}
[class*="col-"] {
  float: left;
}
.col-2 {
  width: 50%;
}
.col-3 {
  width: 33%;
}
.col-3-project {
  width: 33%;
  min-width: 200px;
  // height: 200px;
  display: -webkit-flex;
  display: flex;
  -webkit-flex-wrap: wrap;
  flex-wrap: wrap;
  -webkit-align-content: center;
  align-content: center;
  margin-top: 20px;
  margin-bottom: 20px;
}

.text-overlay {
  width: 100%;
  border: 1px solid #467fbf;
  border-radius: 5px;
  background-color: rgba(0, 0, 100, 0.03);
  margin: 20px;
  padding: 10px;
}
</style>

### We are very happy to welcome our keynote speakers:

<div class="row">
  <div class="col-3-project">
    <div class="text-overlay">
      <div class="row">
      <div class="col-2">
        <p align="center"><a href="https://annakrystalli.me/"><img src="../img/headshots/anna_krystalli.jpg" alt="Anna Krystalli" style="right:15px;width:90%;"></p>
      </div>
      <div class="col-2">
        <p align="center" style="margin-top:50%">
          <a href="https://annakrystalli.me/"><b>Anna Krystalli</b></a><br><a href="https://github.com/annakrystalli" style="margin-right:10px;"><i class="fab fa-github"></i></a><a href="https://twitter.com/annakrystalli"><i class="fab fa-twitter"></i></a>
        </p>
      </div>
      </div>
        <p style="margin:5px;margin-top:20px;">
          <b>Computational Reproducibility, from theory to practice.</b>
        </p>
        <p style="margin:5px;">
          The Reproducibility Crisis has received much attention in the past decade and has even been proposed as the biggest challenge of our academic generation. While debate about the extent and implications of the crisis continues, few would argue that more transparent research, where the raw materials underlying findings are openly shared, would not improve reliability and is, therefore, a reasonable expectation of modern research.
        </p>
        <p align="right" style="margin:5px;">
          <a href="../keynotes/anna_krystalli/index.html">Read more</a>
        </p>
    </div>
  </div>
  <div class="col-3-project">
    <div class="text-overlay">
      <div class="row">
      <div class="col-2">
        <p align="center"><a href="https://statmath.wu.ac.at/~hornik/"><img src="../img/headshots/khornik.png" alt="Kurt Hornik" style="margin:15px;width:90%;"></a></p>
      </div>
      <div class="col-2">
        <p align="center" style="margin-top:50%;"><a href="https://statmath.wu.ac.at/~hornik/"><b>Kurt Hornik</b></a></p>
      </div>
      </div>
      <div class="row">
      <div class="col-2">
        <p align="center">
          <a href="https://www.statistik.tu-dortmund.de/~ligges"><img src="../img/headshots/Ligges_Uwe_verysmall.jpg" alt="Uwe Ligges" style="margin:15px;width:90%;"></a>
        <p>
      </div>
      <div class="col-2">
        <p align="center" style="margin-top:50%;"><a href="https://www.statistik.tu-dortmund.de/~ligges"><b>Uwe Ligges</b></a></p>
      </div>
      </div>
      <p style="margin:5px;margin-top:20px;">
        <b>CRAN 2030</b>
      </p>
      <p style="margin:5px;">
        We will look at how some of CRAN's principles and mechanisms have evolved over the past, and what they might turn into during the next decade.  We provide both our personal views on challenges and opportunities, and try to answer some key questions.
      </p>
      <p align="right" style="margin:5px;">
        <a href="../keynotes/uwe_kurt/index.html">Read more</a>
      </p>
    </div>
  </div>
  <div class="col-3-project">
    <div class="text-overlay">
      <div class="row">
      <div class="col-2">
        <p>
          <a href="https://www.linkedin.com/in/pbiecek/"><img src="../img/headshots/przemek.png" alt="Przemysław Biecek" style="width:90%;margin:15px;"></a>
        </p>
      </div>
      <div class="col-2">
        <p align="center" style="margin-top:50%;">
          <a href="https://www.linkedin.com/in/pbiecek/"><b>Przemysław Biecek</b></a><br>
          <a href="https://www.linkedin.com/in/pbiecek/" style="margin-right:10px;"><i class="fab fa-linkedin-in"></i>
          <a href="https://github.com/pbiecek" style="margin-right:10px;"><i class="fab fa-github"></i>
          <a href="https://medium.com/@ModelOriented/" style="margin-right:10px;"><i class="fab fa-medium"></i>
          </a><a href="https://twitter.com/smarterpoland"><i class="fab fa-twitter"></i></a>
        </p>
      </div>
      </div>
      <p style="margin:5px;margin-top:20px;">
        <b>Talk with your model! Towards the language for exploration and explanation of machine learning models.</b>
      </p>
      <p style="margin:5px;">
        It is getting easier to build complex predictive models. However, without proper verification, these models carry a hidden risk that they will eventually stop working and we won't even notice. To prevent this from happening we need good diagnostic tools to explore, maintain and debug models.
      </p>
      <p align="right" style="margin:5px;">
        <a href="../keynotes/biecek/index.html">Read more</a>
      </p>
    </div>
  </div>
</div>


### Tutorials:

<div class="row">

  <div class="col-3-project">
    <div class="text-overlay">
        <p style="margin:5px;margin-top:20px;">
          <b>Deep Learning with Keras and TensorFlow</b><br>
          Dr. Shirin Elsinghorst (b. Glander)
        </p>
        <p style="margin:5px;margin-top:15px;">
          This workshop begins with a short theoretical part to make everyone familiar with the basics of deep learning, including cross-entropy, loss, activation functions and how weights and biases are optimized with backpropagation and gradient descent. Then we will delve into how to build (deep) neural networks with Keras and TensorFlow in R.
        </p>
        <p align="right" style="margin:5px;">
          <a href="../acc_tutorials/shirin/index.html">Read more</a>
        </p>
    </div>
  </div>

  <div class="col-3-project">
    <div class="text-overlay">
        <p style="margin:5px;margin-top:20px;">
          <b>mlr3</b><br>
          Bernd Bischl & Michel Lang
        </p>
        <p style="margin:5px;margin-top:15px;">
          The mlr3 ecosystem provides a one-stop solution for all machine learning (ML) needs, spanning preprocessing, model learning and evaluation, ensembles, visualization, and hyperparameter tuning.
        </p>
        <p align="right" style="margin:5px;">
          <a href="../acc_tutorials/bernd_michel/index.html">Read more</a>
        </p>
    </div>
  </div>

  <div class="col-3-project">
    <div class="text-overlay">
        <p style="margin:5px;margin-top:20px;">
          <b>Analyzing and visualising spatial and spatiotemporal data cubes</b><br>
          Edzer Pebesma & Martijn Tennekes
        </p>
        <p style="margin:5px;margin-top:15px;">
          Data cubes are a modern way to denote array data, and we focus on array data where some of the dimensions refer to space and time. Examples are spatial raster data, multivariate time series for multiple locations, and time series of raster images such as satellite data or weather predictions. In this workshop we will show a variety of cases where such data arise, and demonstrate how they can be analysed and visualised with R.
        </p>
        <p align="right" style="margin:5px;">
          <a href="../acc_tutorials/pebesma_tennekes/index.html">Read more</a>
        </p>
    </div>
  </div>


</div>


### The following programme is tentative, subject to change.


<div class="row" style="margin:20px;">
  <div class="col-2" style="width:30%;min-width:135px;">
    <center><img src="../img/barcode.png" alt="Eventor Barcode" width="135px"></center>
  </div>
  <div class="col-2" style="width:70%;height:135px;margin-top:26px;">
    <p>
      Scan the barcode with Eventor to have the schedule always with you.
    </p>
    <p>
      <ul>
        <li>
          <a href="https://play.google.com/store/apps/details?id=com.rozdoum.eventor&hl=de">Get Eventor for Android</a>
        </li>
        <li>
          <a href="https://apps.apple.com/de/app/eventor/id562862653">Get Eventor for iOS</a>
        </li>
      </ul>
    <p>
  </div>
</div>

<iframe style="width:100%;height:1100px;" src="https://www.conftool.org/user2020muc/sessions.php"></iframe>


